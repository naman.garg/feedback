from __future__ import unicode_literals


from django.db import models
import django

""" Defines the needed database schema needed for the database. """


class Detail(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    email_id = models.EmailField()
    organisation = models.TextField(blank=True)
    title = models.CharField(max_length=50)
    feed_details = models.TextField(max_length=160)
    rating_choice = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    )
    rating = models.IntegerField(choices=rating_choice, )
    date = models.DateTimeField(default=django.utils.timezone.now, blank=True)



from django.shortcuts import render
from .forms import ContactForm
from .models import Detail
from django.views.generic import View, ListView, FormView
from django.contrib import messages
import datetime

""" This class represents the Feedback Form having template attached   """


class SubmitFormView(FormView):
    form_class = ContactForm
    template_name = 'Core/feedback.html'
    model = Detail
    success_url = "http://localhost:8000/Core/"

    """ This Function defines the action to be taken when form_valid() is called and saves the new value   """
    def form_valid(self, form):
        post = form.save(commit=False)
        post.pub_datetime = datetime.datetime.now()
        post.save()
        messages.info(self.request, "Feedback submitted successfully")
        return super(SubmitFormView, self).form_valid(form)

    """ This function returns the default values and variable when shown to the user   """
    def get_context_data(self, **kwargs):
        context = super(SubmitFormView, self).get_context_data(**kwargs)
        context['current_date'] = datetime.date.today()
        return context

""" Shows the list of database values to the user """


class FormListView(ListView):
    template_name = 'Core/feedback_list.html'
    model = Detail

""" Class based view to show the error to the user """


class PageNotFoundView(View):
    template_name = 'Core/404.html'

    def get(self, request):
        return render(request, self.template_name)

from django.conf.urls import url

from . import views

""" handler for the 404 error page """
# handler404 = views.PageNotFoundView.as_view()

urlpatterns = [
    # url to the form
    url(r'^$', views.SubmitFormView.as_view(), name='index'),
    # url to the list
    url(r'list$', views.FormListView.as_view(), name='list'),

]

